#!/usr/bin/ipython
# Name: ui.py
# Author: Adam Fuller
# Date: 30/4/2016
# Description: A user interface to my barndoor tracker software.
# The angular resolution of the imaging system (i.e. the angle
# subtended by one pixel) determines the
# required tracking accuracy of the tracking mount, which is
# assumed to be limited by the sum of the error of the time source
# to true UTC (ntp) and the tracking error.

import optics
import observing
import L6470
import ntpdc
import time
from terminal_support import get_terminal_size as termsize

# Define the imaging system.
isys = optics.imaging_system(
	sensor=optics.d610_sensor,
	objective=optics.objective(focal_length=0.210, f=5.6))

print "The imaging system is the following:"
print isys
print "The resolution of which is:"
print isys.resolution(), "rad"

# Query the NTP daemon and compute the angular error due to clock
# uncertainty.
ntps = ntpdc.kerninfo()
print
e = ntps['estimated error [s]']

print "The NTP estimated error is", e, 's'
print " which amounts to", e*observing.o, "rad",
print " of angular error at the sidereal rate."
print " which is", e*observing.o/isys.resolution()*100., "% of 1 pixel."

# Connect to the stepper motor driver.
print
print "Connecting to stepper motor."
c = L6470.connection()
c.ResetDevice()

if c.responsive():

	print " OK."

else:

	print " Check stepper motor connection."

	raise Exception

c.ResetDevice()
c.tune()

print
print "What is the current leadnut position?  This defines the position origin."
p0 = float(raw_input('Current leadnut position [m] '))

limits = []
print
print "What are the leadnut travel limits?  Exceeding these limits"
print "will stop the leadscrew and raise a TravelLimitsExceeded"
print "exception."
limits.append(float(raw_input('Lower limit [m] ')))
limits.append(float(raw_input('Upper limit [m] ')))

ls = observing.leadscrew(c, p0=p0, limits=limits)
bd = observing.barndoor(leadscrew=ls, ho=0.05)

print
print "The barndoor instance 'bd' is ready for use."
print "Recommendation: call track_and_report()."

def track_and_report():

	"""
	Starts the barndoor instance tracking and prints a rolling strip
	chart of sorts reporting tracking error in terms of pixels for a
	given imaging system (camera sensor size and resolution, lens
	focal length).
	"""
	print "Ctrl-C for menu.  'X' denotes estimated tracking error."

	if not bd.is_tracking:

		bd.track()

	r = ''

	res = isys.resolution()

	# How many pixels tracking error is important?
	error_px = 0.01

	do_exit = False

	while True:

		try:

			while True:

				if do_exit:

					break

				# Known sources of error are the accuracy of our clock
				# (eclock)...  This is an error magnitude, not a signed or
				# exact value (think error bars) and is therefore a
				# limitation we cannot compensate for in controls.
				eclock = ntps['estimated error [s]']*observing.o

				#...and the difference between our mount's current position
				# and the target position (bd.e).  This value is exact and
				# has a sign since we can calculate it directly.  This is
				# the exact signal our control software is working to
				# minimize and so it should be very small if our software
				# isn't terrible.

				# We're ignoring other important sources of error w.r.t.
				# the final photographic result including polar alignment
				# error and mount geometry error.

				# Having said that, the estimated error range [pixels].
				ee = [(bd.e-eclock)/res, (bd.e+eclock)/res]

				# Compute column positions for the upcoming tracking error
				# graph.  Do this every time through in case the terminal size
				# has changed.
				rows, cols = termsize()
				colm1 = int(cols*1./4)
				colmid  = int(cols*2./4)
				colp1 = int(cols*3./4)

				# Print tick marks.
				line = bytearray(' ')*cols
				line[0] = '['
				line[-1] = ']'
				line[colm1:colm1+9] = '| {:+5.2f}px'.format(-error_px)
				line[colp1:colp1+9] = '| {:+5.2f}px'.format(error_px)
				line[colmid:colmid+5] = '| 0px'

				# The range of columns on the terminal line at which to
				# place Xs depicting the estimated range of tracking error.
				X_marks_the_spotm = min(max(0, colmid +
					int(ee[0]*error_px*(colp1-colmid))), cols-1)
				X_marks_the_spotp = min(max(0, colmid +
					int(ee[1]*error_px*(colp1-colmid))), cols-1)

				X_marks_the_spotp = max(X_marks_the_spotm+1, X_marks_the_spotp)

				line[X_marks_the_spotm:X_marks_the_spotp] = 'X'*(X_marks_the_spotp-X_marks_the_spotm)

				print line[:cols]

				time.sleep(1.0)

		except KeyboardInterrupt:

			print
			print time.ctime()
			print "<Enter> to exit."
			print "r <Enter> returns to error tracking display."

			r = ' '

			while r not in ['r', '']:

				r = raw_input()

			if r == '':

				do_exit = True

				break

			if r == 'r':

				continue

if __name__=='__main__':

	# Run the mega function.
	track_and_report()

	# If we've gotten here, the user wants to go.
	bd.close()
