### optics ###

This repository represents my first steps toward building a barndoor tracker and taking long-exposure photographs of the sky.

* **`optics.py`** This module is about the fundamental geometry of imaging an object.  It includes classes like:
    1. **`objective`** to represent an ideal, focused, light-gathering element or system of elements of a given `focal_length` and *`f`*-ratio.
    2. **`sensor`** to represent a rectangular array of pixels, and
    3. **`imaging_system`** which takes one of each of the above and conceptually places the latter on the imaging plane of the former.  The system's field of view and angular resolution may be queried.


* **`observing.py`** This module is about the motion required to match the apparent motion of celestial objects.  Its main features are:

    1. the `barndoor` class which describes the geometry of a one-axis, polar aligned mount a.k.a. a barndoor tracking mount, and
    2. the `leadscrew` class which handles the bidirectional translation of the linear motion of nut on a threaded rod of uniform pitch to and from stepper motor movements (see my `L6470` repository).