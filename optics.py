# Name: optics.py
# Author: Adam Fuller
# Date: 19/6/2015
# Description: Calculate things to do with image sensors, lenses,
# etc.

import math

def solid_angle(thetah, thetav):
	"""
	Given horizontal and vertical half-angles [radians], returns the subtended
	solid angle [steradians].
	"""

	return 2*thetah*(-math.cos(math.pi/2+thetav) + math.cos(math.pi/2-thetav))

class sensor():

	"""
	A rectangular image sensor.
	"""

	def __init__(self, width=None, height=None, pixels_wide=None,
			pixels_high=None):

		""" [meters] """
		self.width = width
		self.height = height

		""" [pixels] """
		self.pixels_wide = pixels_wide
		self.pixels_high = pixels_high
	
	def __str__(self):

		return '{:.0f} x {:.0f} pixels ({:.1f}M), {:.1f}mm x {:.1f}mm ({:.1f}mm^2)'.format(
			self.pixels_wide,
			self.pixels_high,
			self.pixel_count()/1e6,
			self.width*1000,
			self.height*1000,
			self.area()*1e6)

	def __repr__(self):

		return self.__str__()

	def pixel_count(self):
		
		return self.pixels_wide * self.pixels_high

	def horizontal_pitch(self):
		return self.width / self.pixels_wide

	def vertical_pitch(self):
		return self.height / self.pixels_high

	def area(self):

		""" [m**2] """

		return self.width * self.height

	def pixels(self):

		""" Total number of pixels. """

		return self.pixels_wide * self.pixels_high

	def circumscribed(self):

		""" Return the diameter of a circle circumscribed about the
		sensor area. """

		return 2*((self.width/2.0)**2 + (self.height/2.0)**2)**0.5

class objective():

	"""
	An ideal objective lens focused at infinity.
	"""

	def __init__(self, focal_length=None, f=None):

		self.focal_length = focal_length
		self.f = f # e.g. for 1:4.5 pass 4.5
	
	def __str__(self):

		return '{:.0f}mm f/{:.1f}'.format(
			self.focal_length*1000,
			self.f)

	def __repr__(self):

		return self.__str__()

	def aperture(self):
		
		return self.focal_length / self.f

class imaging_system():

	def __init__(self, sensor=None, objective=None):

		"""
		An objective and a sensor together comprise an imaging system.
		The centerpoint of the sensor is assumed to lie on the optical
		axis of the objective.  The sensor is assumed normal to the
		optical axis.
		"""

		self.sensor = sensor

		self.objective = objective

	def __str__(self):

		return str(self.sensor)+'\n'+str(self.objective)

	def __repr__(self):

		return self.__str__()
	
	def halfheight(self):

		"""
		Return the half-height of the object in radians to vertically fill
		the center of the sensor.
		"""

		return math.atan(self.sensor.height/2.0/self.objective.focal_length)
	
	def halfwidth(self):

		"""
		Return the half-width of the object in radians to span the
		sensor.
		"""

		return math.atan(self.sensor.width/2.0/self.objective.focal_length)

	def solid_angle(self):

		"""
		Return the solid angle [steradians] of the image.
		"""

		return solid_angle(self.halfwidth(), self.halfheight())
	
	def resolution(self):

		"""
		Return the object angle subtended by one pixel.  Assume the
		sensor vertical and horizontal pitches are equal.
		"""

		return math.atan(
			self.sensor.horizontal_pitch()/self.objective.focal_length)

	def project_pixel(self, x, y):

		"""
		Return the angular coordinates in the object space
		corresponding to a location on the sensor x pixels from the
		left column of pixels and y pixels from the bottom row of
		pixels.
		"""

		# Calculate sensor location in meters
		xmc = -self.sensor.width/2.0 + self.sensor.horizontal_pitch()*x
		ymc = -self.sensor.height/2.0 + self.sensor.vertical_pitch()*y

		# Calculate pixel center on the object
		xa = -math.atan(xmc/self.objective.focal_length)
		ya = -math.atan(ymc/self.objective.focal_length)

		return [xa, ya]

# Nikon D610 sensor
d610_sensor = sensor(width=0.036, height=0.024,
	pixels_wide=6000, pixels_high=4000)

# Nikon D610
d610 = imaging_system(
	sensor(width=0.036, height=0.024,
	pixels_wide=6000, pixels_high=4000),
	objective(focal_length=0.050, f=1.4))

# Canon Powershot S100
s100 = imaging_system(
	sensor(width=0.0076, height=0.0057,
	pixels_wide=4000, pixels_high=3000),
	objective(focal_length=0.0260, f=5.6))
