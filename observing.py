# Name: observing.py
# Author: Adam Fuller
# Date: 10/8/2015
# Description: Observational astronomy stuff.

import math
import time
import threading
import multiprocessing as mp

# Standard acceleration due to Earth's gravity.
g = 9.80665 # m/s**2

# The ratio of the duration of a sidereal second to an SI second.
# See https://en.wikipedia.org/wiki/Sidereal_time and
# http://adsabs.harvard.edu/full/1982A%26A...105..359A.  To 5
# decimal places this is equal to 365/366.
sr  = 0.997269566329084

# An approximate sidereal angular velocity [rad/s].
o = 2*math.pi/(24*60*60*sr)

class leadscrew():

	def __init__(self, c, pd1=0.0028, l=0.200, p0=0.0, sense=1,
			limits=None):

		"""
		Instances of this class represent a device which converts
		rotation into axial translation.  Without position feedback,
		you should pass a meaningful value for p0, otherwise the
		position will only be relative to the starting point.
		
		Parameters
		----------
		c : L6470.connection instance
			A connection to the stepper motor driving the leadscrew.
		
		pd1 : float
			leadscrew advance per rev [m].

		l : float
			leadscrew stroke [m].

		p0 : float
			The starting position of the leadnut, positive away
			from the motor. [m]

		sense : int in [-1,1]
			This parameter encapsulates the combined leadscrew handedness and
			stepper wiring polarity. Pass 1 if a positive step moves the leadnut
			away from the motor and -1 otherwise.

		limits : [minimum position, maximum position] or None
			If a list is passed, the values are interpreted as the
			minimum and maximum values of leadnut position as determined
			by self.current_pos.  A parallel thread will be executed
			which periodically checks if these limits are exceeded, and
			if so, stops the leadscrew and raises an Exception.

		"""

		self.c = c
		self.pd1 = pd1
		self.p0 = p0
		self.l = l

		assert sense in [1, -1]
		self.sense = sense

		if limits:

			"""
			Special setup if the user has specified travel limits that
			should be respected.
			"""

			# Make the limits an attribute of this leadscrew.
			self.limits = limits

			# This property should reflect whether we're in an error
			# state or not.
			self.limit_error = False

			#  Call .set() to allow this leadscrew's TravelLimitsExceeded
			#  thread to exit.
			self.disable_TravelLimitsExceeded = threading.Event()

			# If limits of travel have been provided, check that they
			# aren't exceeded going forward, and raise an exception
			# (after stopping, of course) if they are.

			def start_limit_checker():

				"""
				Clear any standing limit error, re-join any existing
				limit checker threads, create a new thread and start it.
				"""

				self.limit_error = False

				self.disable_TravelLimitsExceeded.clear()

				class TravelLimitsExceeded(Exception):

					"""
					A special Exception to raise if this leadscrew's travel
					limits are exceeded.
					"""

					pass

				def limit_checker(ls):

					"""
					A function which includes an infinite loop, checking if the
					current position is outside some limits.  Meant to be
					called in a parallel thread.
					"""

					while not ls.disable_TravelLimitsExceeded:
					#while True:

						if (
							ls.current_pos>max(self.limits) and
								ls.current_speed>0
							) or (
							ls.current_pos<min(self.limits) and
								ls.current_speed<0):

							# If so, stop the leadscrew, set a flag, and raise and
							# exception.

							if hasattr(ls.c, 'stoptrimmer'):

								ls.c.stoptrimmer.set()

							if hasattr(self, 'ptrimmer'):

								ls.c.ptrimmer.join() # join any trimmer thread.

							ls.c.HardHiZ()

							ls.limit_error = True

							raise TravelLimitsExceeded

						# This needs to be short enough to prevent problems
						# happening for a slight overrun.
						time.sleep(0.5)

				self.checker = threading.Thread(target=limit_checker, args=(self,))
				self.checker.start()

			def stop_limit_checker():

				"""
				Calling this function causes the limit_checker thread to
				break out and exit, by raising a shared-memory flag seen in
				the checker's thread.
				"""

				self.disable_TravelLimitsExceeded.set()

			# Put the limit checker start/stop functions on the
			# leadscrew's namespace for later calling.
			self.start_limit_checker = start_limit_checker
			self.stop_limit_checker = stop_limit_checker

			# Start initially.
			self.start_limit_checker()

	@property
	def current_speed(self):

		"""
		Current leadnut speed [m/s] (positive is extending).
		"""

		#      rad                            m        rev     sign
		#      s                              rev      rad
		return self.c.monitor['Vrad'].value * self.pd1 /2/math.pi *   self.sense

	@property
	def current_pos(self):

		"""
		Current leadnut position [m].
		"""

		#      rad                            sign         m    rev
		#                                                  rev  rad
		return self.c.monitor['prad'].value * self.sense * self.pd1/2/math.pi + self.p0

	def close(self):

		if hasattr(self, 'checker'):

			"""
			If the limit_checker thread is present, set it's "shutdown"
			flag.
			"""

			self.stop_limit_checker()


		# Tell the stepper motor connection to close.
		self.c.close()

	def Run(self, V):

		"""
		Set the target leadnut velocity [m/s].  Positive is extending.

		Parameters
		----------
		rate : float
			Signed target signed leadnut velocity [m/s].

		"""

		#          m                rev
		#          s                m
		self.c.Run(V * self.sense / self.pd1, units='rev/s')

	def SoftStop(self):

		"""
		Ramp speed to zero and hold.
		"""

		self.c.SoftStop()

	def SoftHiZ(self):

		"""
		Ramp speed to zero and make bridges high impedance ("High Z").
		Terminology from L6470.
		"""

		self.c.SoftHiZ()

	def Move(self, x):

		"""
		Move the leadnut by x meters.  Positive is extending.

		Parameters
		----------
		x : float
			Signed target leadnut change in position [m/s].

		"""

		#          m                rev
		#                          m
		self.c.Move(x * self.sense / self.pd1, units='rev')

	def GoTo(self, p):

		"""
		GoTo (i.e. move the leadnut) to position x [].  This is
		relative to the zero position at the time of leadscrew
		instantiation.

		Parameters
		----------
		x : float
			Signed target leadnut absolute position target [m].

		"""

		self.Move(p - self.current_pos)

	def Runf(self, f, gett, testing=False):

		"""
		Start a parallel thread which will vary the speed of the
		leadscrew to minimize the axial position error to a function of
		time "f" which yields a target leadscrew position [m].
		"""

		self.c.RunTrimmedf(
			fpos = lambda: self.current_pos,
			pt = f,
			frun = lambda V: self.Run(V),
			gett = gett,
			testing=testing)

def isoceles_law_of_cosines(a,gamma):

	"""
	Given a and gamma of an isoceles triangle with legs a,a,c and angles
	alpha,alpha,gamma, return leg c.

	Parameters
	----------
	a : float
		The length of the two similar legs of an isoceles triangle [m].

	gamma : float
		The angle between the two similar legs, or equivalently, the angle
		opposite leg c [rad].
	"""

	def sign(x): return math.copysign(1,x)

	return sign(gamma)*(2*a**2*(1-math.cos(abs(gamma))))**0.5

def isoceles_law_of_cosines_inv(a,c):

	"""
	Given a and c of an isoceles triangle with legs a,a,c and angles
	alpha,alpha,gamma, return angle gamma.

	Parameters
	----------
	a : float
		The length of the two similar legs of an isoceles triangle [m].

	c : float
		The length of the odd leg of an isoceles triangle [m].

	"""

	return math.acos(1-c**2/(2*a**2))

def vsky(dec):
	
	"""
	The apparent great-circle angular velocity magnitude of a point
	in the sky viewed from earth.  Depends on declination only.

	Returns
	-------
	float [rad/s]
	"""

	assert abs(dec) <= math.pi/2

	return o * math.sin(math.pi/2 - dec)

class barndoor():

	def __init__(self, rd=0.3, m=None,
			rm=None, pole='S', ho=0.010, action='lift', leadscrew=None):

		"""
		Instances of this class represent an isoceles-mount barndoor tracker
		driven by a stepper motor and leadscrew.  See
		https://en.wikipedia.org/wiki/Barn_door_tracker.

		A note on time:
		The tracking accuracy can only ever be as good as the system
		clock frequency relative to UTC.  Use NTP.

		This class includes a clock in addition to the tracker.  The
		clock provides the target for motion.  The clock may be
		controlled independently of motion.

                                Method or attribute
                                --------------------
		Description     Clock         Motion
                -----------     -----         ------
		Make it go	start         track
		Make it stop    clear         stop
                Is is going?    has_target    is_tracking

		is_tracking implies has_target.  Calling track calls start if has_target is False.

		Parameters
		----------
		rd : float
			Radial distance from the polar axis at which the leadscrew acts when
			the mount is closed i.e the isoceles triangle leg length [m].

		m : float
			Payload mass [kg].

		rm : float
			Horizontal distance of payload center of mass from the polar axis
			[m].

		pole : ['S', 'N']
			The celestial pole to which the mount's polar axis is aligned.

		ho : float
			The "hypotenuse offset".  ho + leadscrew_position =
			leg_c_length.  A geometric property of the mount.  Whatever
			the leadnut position would be when the tracker angle is zero,
			take the opposite.

		action : ['lift', 'lower']
			Whether the leadscrew should lift (gamma increasing) or lower (gamma
			reducing) the payload to track the sky.

		leadscrew : leadscrew instance
			The leadscrew instance driving the mount.

		"""

		assert pole in 'NS'
		assert action in ['lift', 'lower']

		self.rd = rd
		self.m = m
		self.rm = rm
		self.pole = pole
		self.action = action
		self.has_target = False
		self.is_tracking = False
		self.leadscrew = leadscrew
		self.hypoffset = ho

	def close(self):

		self.stop()

		return self.leadscrew.close()

	def start(self):

		"""
		Start a time from which the target angle gammat to match
		sidereal motion may be calculated.  This uses the system clock and assumes it
		to be very close to UTC, which is a fair assumption if you have set up ntp.
		This only provides the moving target position.  Use the track method to move
		towards this target.  Query the running property to see if the
		clock is running.  Use the clear method to stop the clock.
		"""

		if self.has_target:

			raise Exception('Clock already running.')

		self.t0 = time.time()

		self.has_target = True

		# Target angular velocity, signed [rad/s].
		self.ot = o*{'lift': 1, 'lower': -1}[self.action]

		# Mount angle at the starting point.
		self.gammastart = self.gamma

	@property
	def t(self):

		"""
		The duration since tracking began [s].
		"""

		return time.time() - self.t0

	@property
	def legc(self):

		return self.leadscrew.current_pos + self.hypoffset

	@property
	def e(self):

		"""
		If tracking, return the difference between the target and actual angle.
		"""

		if self.is_tracking:

			return self.gammat(self.t) - self.gamma

		else:

			return None

	@property
	def gamma(self):

		"""
		The current mount angle [rad].
		"""

		return isoceles_law_of_cosines_inv(self.rd, self.legc)

	def gammat(self, t):

		"""
		The target mount angle [rad] at time t.
		"""

		return t * self.ot + self.gammastart

	def ct(self, t):

		"""
		The target leg c length [m] at time t.
		"""

		return isoceles_law_of_cosines(self.rd, self.gammat(t))

	def lspt(self, t=None):

		"""
		The target lead screw nut position [m] at time t.  If called
		without an argument, return the target position right now.
		"""

		if t == None:

			t = self.t

		return self.ct(t) - self.hypoffset

	def track(self, testing=False):

		"""
		Set leadscrew ls running toward a sidereal-rate target.
		"""

		if self.is_tracking:

			raise Exception('Already tracking')

		if self.has_target:
		
			print "Tracking existing target."

		else:

			print "Tracking new target."

			self.start()

		self.leadscrew.Runf(
			f = self.lspt, 
			gett = lambda: self.t,
			testing=testing,
			)

		self.is_tracking = True

	def remaining(self):

		"""
		Returns the total tracking seconds left, as determined by the
		remaining actuator stroke.
		"""

		# Residual stroke
		rstroke = self.leadscrew.l - self.leadscrew.current_pos

		# Angle at end of stroke
		gammaend = isoceles_law_of_cosines_inv(self.rd,self.leadscrew.l+self.hypoffset)

		# Angle now
		dgamma = gammaend - self.gamma

		# Return remaining time (remaining mount angle divided by sidereal rate).
		return dgamma/abs(o)

	def stop(self):

		"""
		Stop the tracker moving.  The clock continues so that the same
		target may be returned to later.  Use clear to stop the clock
		(which will also stop the tracker moving if it is).
		"""

		self.leadscrew.c.stoptrimmer.set()

		self.leadscrew.c.SoftHiZ()

		self.is_tracking = False

	def clear(self):

		if self.is_tracking:

			raise Exception('Call .stop first.')

		self.has_target = False

		self.t0 = None
